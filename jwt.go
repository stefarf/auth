package auth

import (
	"errors"
	"time"

	"bitbucket.org/stefarf/iferr"
	"github.com/robbert229/jwt"
)

type (
	Auth struct{ algorithm jwt.Algorithm }
)

// var algorithm = jwt.HmacSha256("LF7uXyDnuuKoUYonzboPgCQ0h33Nc7P8UWZ9Y5nW")
var (
	ErrInvalidToken = errors.New("invalid token")
	ErrUnexpected   = errors.New("unexpected error")
)

func New(key string) *Auth { return &Auth{jwt.HmacSha256(key)} }

func (auth *Auth) CreateJWT(daysNotBefore, daysExpire int, email string) string {
	// Set claims
	claims := jwt.NewClaim()
	claims.SetTime("nbf", time.Now().AddDate(0, 0, daysNotBefore)) // Not before
	claims.SetTime("exp", time.Now().AddDate(0, 0, daysExpire))    // Expire
	claims.Set("email", email)

	// Sign the claims
	token, err := auth.algorithm.Encode(claims)
	iferr.Panic(err)
	return token
}

func (auth *Auth) VerifyAndReadToken(token string) (email string, err error) {
	// Validate token
	if auth.algorithm.Validate(token) != nil {
		return "", ErrInvalidToken
	}

	// Read the claims
	claims, err := auth.algorithm.Decode(token)
	if err != nil {
		return "", err
	}
	ema, err := claims.Get("email")
	if err != nil {
		return "", err
	}

	switch ema.(type) {
	case string:
		return ema.(string), nil
	default:
		return "", ErrUnexpected
	}
}
